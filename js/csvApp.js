var app = angular.module('csvApp', []);

app.controller('csvAppCtrl',function($scope){

   var fileRecords = {};

   $scope.clearFilter = function(){
        $scope.records = $scope.temp;
   }

    $scope.filter = function(){

        $scope.errorMessage = undefined;

        if($scope.temp!=undefined){
            $scope.records = $scope.temp;
        }

       var filterValue =  $("#operatorText").val();
       var operator = $("#operator").val();
       if(operator.length>2){
            $scope.errorMessage = "Select Operator"
            return;
       }
       var filteredRecords;
       switch(operator){
            case 'eq':
                    filteredRecords = $scope.records.filter(function (el) {
                      return  el.Issuecount == filterValue;
                    });
                    break;
            case 'lt':
                    filteredRecords = $scope.records.filter(function (el) {
                      return  el.Issuecount < filterValue;
                    });
                    break;
            case 'nt':
                    filteredRecords = $scope.records.filter(function (el) {
                      return  el.Issuecount != filterValue;
                    });
                    break;

            case 'gt':
                    filteredRecords = $scope.records.filter(function (el) {
                      return  el.Issuecount > filterValue;
                    });
                    break;
       }
       $scope.temp = $scope.records;
       $scope.records = filteredRecords;
    };

   $scope.uploadFile = function(){
      var file = event.target.files[0];
      var fileName = file.name;
      $('.custom-file-label').html(fileName);
         var r = new FileReader();
         r.onload = function(e) {
             var contents = e.target.result;
             console.log(contents);
             var result = getRecords(contents);
             $scope.$apply(function () {
               $scope.tableHeader = result[0];
               $scope.records = result[1];
             });
         };

       r.readAsText(file);
   };



   var getRecords = function(csv) {
       var rows = csv.split("\n");
       var result = [];
       var records = [];
       var headers = [];
       var tableHeader = [];

       for(var rowIndex = 0; rowIndex<rows.length; rowIndex++){
            var row = rows[rowIndex];
            var columns = row.split(",");
            if(rowIndex>0){
                var record = {};
                for(var columnIndex = 0; columnIndex<columns.length;columnIndex++){
                    var column = columns[columnIndex];
                    record[headers[columnIndex]] = column;
                }
                records.push(record);
            }else{

                for(var headerIndex = 0; headerIndex<columns.length; headerIndex++){
                   tableHeader.push(columns[headerIndex])
                   headers.push(columns[headerIndex].replace(/\s/g, "")) ;
                }
            }
       }
       console.log(records);
        var newArray = records.filter(function (el) {
            return  el.Issuecount > 3
        });

        console.log(newArray);
        result.push(tableHeader,records);
        return result;
   }

});


app.directive('fileOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeFunc = scope.$eval(attrs.fileOnChange);
      element.bind('change', onChangeFunc);
    }
  };
});
